package com.example.Template.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.Template.R
import com.example.Template.data.local.room.entity.JobFavEntity
import com.example.Template.databinding.FragmentMainBinding
import com.example.Template.ui.base.BaseFragment
import com.example.Template.util.Resource
import com.example.Template.util.Utility
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class MainFragment : BaseFragment() {

    val TAG: String = "MainFragment"
    val viewModel: MainViewModel by viewModels()
    lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Call API
        viewModel.uploadFileLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                Resource.Status.LOADING -> {
                    progressBar!!.show()
                    Log.d(TAG, "LOADING ${it.message}")
                }
                Resource.Status.SUCCESS -> {
                    Log.i(TAG, "SUCCESS ${it.message}")
                }
                Resource.Status.ERROR -> {
                    progressBar!!.dismiss()
                    Log.e(TAG, "ERROR ${it.message}")
                }
            }
        })

        onInsertFavJob()
    }

    fun onInsertFavJob() {
        CoroutineScope(Dispatchers.IO).launch {
            var row = viewModel.onInsertFavJob(JobFavEntity(null, "test-01", "{{\n" + "      \"message\": \"Request failed with status code 400\"}"))
            if (row > 0) {
                withContext(Dispatchers.Main) {
                    Utility.showToast(requireContext(), "Saved successfully!")
                }
            }
            Utility.showErrorLog(TAG, "$row inserted")

        }
    }

}