package com.example.Template.ui.main

import androidx.lifecycle.*
import com.example.Template.data.local.room.entity.JobFavEntity
import com.example.Template.util.Resource
import com.exmaple.Template.data.remote.response.FileUploadResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: MainRepository,
) : ViewModel() {

    val TAG = "MainViewModel"

    val uploadFileLiveData: MutableLiveData<Resource<FileUploadResponse>> = MutableLiveData()

    fun onFileUpload(pdfFile: File) {

        val file = MultipartBody.Part.createFormData(
            "file",
            pdfFile.name,
            pdfFile.asRequestBody("application/pdf".toMediaType())
        )
        viewModelScope.launch {
            repository.uploadFile(file)
                .onStart {
                    withContext(Dispatchers.Main) {
                        uploadFileLiveData.value = Resource(
                            Resource.Status.LOADING,
                            null,
                            "Loading..."
                        )
                    }
                }.catch { error ->
                    withContext(Dispatchers.Main) {
                        uploadFileLiveData.value = Resource(
                            Resource.Status.ERROR,
                            null,
                            error.message.toString()
                        )
                    }
                }.collect { result ->
                    withContext(Dispatchers.Main) {
                        uploadFileLiveData.value = Resource(
                            Resource.Status.SUCCESS,
                            result,
                            "File uploaded successfully!"
                        )
                    }
                }
        }
    }

    fun onCheckFavJob(id: String): JobFavEntity {
        return repository.checkFavJob(id)
    }

    fun onGetFavJobs(): LiveData<List<JobFavEntity>>{
        return repository.getFavJobs()
    }

    fun onInsertFavJob(job: JobFavEntity): Long{
        return repository.insertFavJob(job)
    }

    fun onRemoveFavJob(job: JobFavEntity){
        return repository.removeFavJob(job)
    }

}