package com.example.Template.ui.main

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.Template.data.local.room.AppDao
import com.example.Template.data.local.room.entity.JobFavEntity
import com.example.Template.data.remote.endpoint.ApiService
import com.exmaple.Template.data.remote.response.FileUploadResponse
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import okhttp3.MultipartBody

class MainRepository(
        private val apiService: ApiService,
        private val appDao: AppDao,
        private val gson: Gson,
        private val context: Context
) {

    suspend fun uploadFile(file: MultipartBody.Part) = flow<FileUploadResponse> {
        flowOf(apiService.uploadFile(file))
            .catch {
                throw Exception(it.message)
            }.map {
                emit(it)
            }.collect()
    }.flowOn(Dispatchers.IO)

    fun checkFavJob(id: String): JobFavEntity {
        return appDao.checkFavJob(id)
    }

    fun getFavJobs(): LiveData<List<JobFavEntity>> {
        return appDao.getFavJobs()
    }

    fun insertFavJob(job: JobFavEntity): Long {
        return appDao.insertFavJob(job)
    }

    fun removeFavJob(job: JobFavEntity) {
            appDao.removeFavJob(job)
    }
}