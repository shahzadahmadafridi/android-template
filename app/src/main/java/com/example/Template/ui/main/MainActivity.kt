package com.example.Template.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.Template.R
import com.example.Template.util.Constants
import com.example.Template.util.FragmentStack
import dagger.hilt.android.AndroidEntryPoint
import java.lang.StringBuilder

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    val TAG: String = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FragmentStack.addFragmentToContainer(R.id.main_container, supportFragmentManager,
                MainFragment(), "MainFragment", false)
    }

    override fun onBackPressed() {

        Log.e(TAG,"----------------------------------onBackPressed---------------------------------")

        var builder = StringBuilder()
        builder.append("\n")

        val fragList = supportFragmentManager.fragments
        var lastFrag = fragList.lastOrNull()
        if (lastFrag!!.tag!!.contentEquals("com.bumptech.glide.manager")){
            builder.append("lastFrag: com.bumptech.glide.manager removed").append("\n")
            fragList.remove(lastFrag)
            lastFrag = fragList.lastOrNull()
        }

        builder.append("lastFrag: ${lastFrag!!.tag}").append("\n")

        if (Constants.homeFragments.contains(lastFrag.tag) && lastFrag.isVisible) {
            //Primary fragment
            moveTaskToBack(true)
            builder.append("lastFrag: Visible").append("\n")
        } else {
            if (supportFragmentManager.backStackEntryCount != 0) {
                builder.append("lastFrag: Not Visible - popBackStack").append("\n")
                supportFragmentManager.popBackStack()
            } else {
                builder.append("lastFrag: Not Visible - onBackPressed").append("\n")
                super.onBackPressed()
            }
        }

        Log.e(TAG,builder.toString())
    }
}