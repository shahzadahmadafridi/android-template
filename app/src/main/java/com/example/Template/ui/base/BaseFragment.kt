package com.example.Template.ui.base

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.Template.R
import com.example.Template.util.Utility

abstract class BaseFragment() : Fragment() {

    var parentActivity: AppCompatActivity? = null
    var progressBar: Dialog? = null
    var notifyDialog: Dialog? = null
    var statusDialog: Dialog? = null
    var statusDialogIcon: ImageView? = null
    var statusDialogTitle: TextView? = null
    var statusDialogMessage: TextView? = null
    var statusDialogMessagell: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        parentActivity = activity as AppCompatActivity?

        progressBar =  Utility.onProgressDialog(requireContext(), R.layout.custom_progressbar)

        notifyDialog = Utility.onCreateDialog(requireContext(), R.layout.notify_dialog_layout, true)!!
        notifyDialog!!.window!!.attributes.windowAnimations = R.style.FullScreenDialogStyle; //style id
        notifyDialog!!.window!!.setGravity(Gravity.TOP)

        statusDialog = Utility.onCreateDialog(requireContext(), R.layout.status_dialog, true)!!
        statusDialog!!.window!!.attributes.windowAnimations = R.style.FullScreenDialogStyle; //style id
        statusDialog!!.window!!.setGravity(Gravity.TOP)
        statusDialogIcon = statusDialog!!.findViewById(R.id.status_dialog_icon)
        statusDialogTitle = statusDialog!!.findViewById(R.id.status_dialog_title)
        statusDialogMessage = statusDialog!!.findViewById(R.id.status_dialog_message)
        statusDialogMessagell = statusDialog!!.findViewById(R.id.status_dialog_msg_ll)

//        setNavTitle()

    }

    fun onStatusDialog(title: String, message: String?, icon: Int, isHide: Boolean){
        statusDialogIcon!!.setImageResource(icon)
        statusDialogTitle!!.text = title
        message?.let {
            statusDialogMessage!!.text = message
            statusDialogMessage!!.visibility = View.VISIBLE
            statusDialogMessagell!!.visibility = View.VISIBLE
        } ?: run {
            statusDialogMessage!!.text = ""
            statusDialogMessage!!.visibility = View.GONE
            statusDialogMessagell!!.visibility = View.GONE
        }
        statusDialog!!.show()

        if (isHide){
            Handler(Looper.myLooper()!!).postDelayed({
                statusDialog!!.dismiss()
            },3000)
        }
    }

//    abstract fun setNavTitle()
// Testing CI/CD

}
