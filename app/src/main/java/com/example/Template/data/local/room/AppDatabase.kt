package com.example.Template.data.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.Template.data.local.room.AppDao
import com.example.Template.data.local.room.entity.*

@Database(entities = [JobFavEntity::class, UserEntity::class], version = 3, exportSchema = false)
abstract class AppDatabase: RoomDatabase(){

    abstract fun flexDao(): AppDao

}