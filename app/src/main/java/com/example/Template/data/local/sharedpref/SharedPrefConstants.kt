package com.example.Template.data.local.sharedpref

import android.content.SharedPreferences

object SharedPrefConstants {

    val SHARED_PREF = "app_pref"
    val TOKEN_KEY = "token"

    fun saveStringValue(sharedPrefEditor: SharedPreferences.Editor, key: String, value: String){
        sharedPrefEditor.putString(key,value).apply()
    }

    fun saveBooleanValue(sharedPrefEditor: SharedPreferences.Editor, key: String, value: Boolean){
        sharedPrefEditor.putBoolean(key,value).apply()
    }

    fun getStringValue(sharedPreferences: SharedPreferences, key: String): String?{
        return sharedPreferences.getString(key,"")
    }

    fun getBooleanValue(sharedPreferences: SharedPreferences, key: String): Boolean{
        return sharedPreferences.getBoolean(key,false)
    }
}