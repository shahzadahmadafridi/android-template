package com.example.Template.data.remote.endpoint

import com.example.Template.data.remote.response.ContentResponse
import com.exmaple.Template.data.remote.response.FileUploadResponse
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

/**
 * REST API access points
 */

interface ApiService {

    @GET
    suspend fun content(@Url url: String): ContentResponse

    @Multipart
    @POST("uploads/file")
    suspend fun uploadFile(@Part pdf: MultipartBody.Part): FileUploadResponse

    @Streaming
    @GET
    suspend fun downloadFile(@Url fileUrl:String): Response<ResponseBody>

}