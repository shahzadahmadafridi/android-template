package com.example.Template.data.remote.response

data class ContentResponse(
    val title: String,
    val body: String
)
