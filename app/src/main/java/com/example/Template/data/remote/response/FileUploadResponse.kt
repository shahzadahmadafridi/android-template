package com.exmaple.Template.data.remote.response

data class FileUploadResponse(
    val fileName: String,
    val fileReferenceNumber: String
)