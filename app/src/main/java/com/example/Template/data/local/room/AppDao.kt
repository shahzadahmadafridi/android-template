package com.example.Template.data.local.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.Template.data.local.room.entity.JobFavEntity
import com.example.Template.data.local.room.entity.UserEntity

@Dao
interface AppDao {

//    @Transaction
//    fun clearAndCacheBundleType(category: CategoryEntity) {
//        clearCategory()
//        insertCategory(category)
//    }

    //User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserEntity) : Long

    @Query("SELECT * FROM user where id = :id")
    fun getUserResponse(id: String): LiveData<UserEntity>

    //Fav Job

    @Query("SELECT * FROM job_fav where job_id = :id")
    fun checkFavJob(id: String): JobFavEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavJob(job: JobFavEntity) : Long

    @Query("SELECT * FROM job_fav")
    fun getFavJobs(): LiveData<List<JobFavEntity>>

    @Delete
    fun removeFavJob(job: JobFavEntity)

}