package com.example.Template.data.local.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntity(
    @PrimaryKey
    @ColumnInfo(name="id")
    val id : Int,
    @ColumnInfo(name="response")
    val response : String
)