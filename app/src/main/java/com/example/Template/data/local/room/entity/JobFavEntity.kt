package com.example.Template.data.local.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Job_fav")
data class JobFavEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    val id : Int? = null,
    @ColumnInfo(name="job_id")
    val job_id : String,
    @ColumnInfo(name="response")
    val response : String
)