package com.example.Template.di

import android.content.SharedPreferences
import android.util.Log
import com.example.Template.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(sharedPreferences: SharedPreferences): OkHttpClient {
        return OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }).addInterceptor(Interceptor { chain ->
                    val authToken = sharedPreferences.getString(Constants.ACCESS_TOKEN, "")
                    Log.e("provideOkHttpClient",authToken!!)
                    val original = chain.request()
                    val request = original.newBuilder()
                            .addHeader("Authorization", "Bearer $authToken")
                            .addHeader("App-Name","flex") //CMS Api
                            .addHeader("X-Meili-API-Key","empireholding") // Meilie Search Api
                            .build()
                    chain.proceed(request)
                })
                .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
    }

}