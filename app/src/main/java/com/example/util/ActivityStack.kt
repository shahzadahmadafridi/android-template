package com.example.Template.util

import android.content.Context
import android.content.Intent
import com.example.Template.ui.main.MainActivity

object ActivityStack {

    fun startSplashActivity(context: Context) {
        val intent = Intent(context, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
    }
}