package com.example.Template.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

object FragmentStack {

    fun replaceFragmentToContainer(container: Int, fragmentManager: FragmentManager, fragment: Fragment, tag: String, isAnimation: Boolean) {
        val transaction = fragmentManager.beginTransaction()
        if (isAnimation)
            transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        transaction.replace(container, fragment, tag)
        //onBackPressed work If uncomment.
        transaction.addToBackStack(tag)
        transaction.commit()
    }

    fun replaceFragmentToContainerFade(container: Int, fragmentManager: FragmentManager, fragment: Fragment, tag: String, isAnimation: Boolean) {
        val transaction = fragmentManager.beginTransaction()
        if (isAnimation)
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
        transaction.replace(container, fragment, tag)
        //onBackPressed work If uncomment.
        transaction.addToBackStack(tag)
        transaction.commit()
    }

    fun addFragmentToContainer(container: Int, fragmentManager: FragmentManager, fragment: Fragment, tag: String, isAnimation: Boolean) {
        val transaction = fragmentManager.beginTransaction()
        if (fragmentManager.findFragmentByTag(tag) == null) { // No fragment in backStack with same tag..
            transaction.add(container, fragment, tag)
            transaction.addToBackStack(tag)  //onBackPressed work If uncomment.
            // transaction.setCustomAnimations(R.anim.sliding_in_left, R.anim.sliding_out_right)
            transaction.commit()
        } else {
            /*
            for (frag in fragmentManager.fragments)
                transaction.hide(frag)
            transaction.show(fragmentManager.findFragmentByTag(tag)!!).commit()
            */
            transaction.replace(container, fragment, tag)
            transaction.addToBackStack(tag)  //onBackPressed work If uncomment.
            // transaction.setCustomAnimations(R.anim.sliding_in_left, R.anim.sliding_out_right)
            transaction.commit()
        }
    }

}
