package com.example.Template.util

import java.util.regex.Pattern

object Constants {

    const val BASE_URL = "https://test-api.com/"
    const val LOCAL_DATABASE_NAME = "appDatabase"
    const val SHARED_PREF_NAME = "app"
    const val IS_USER_LOGIN = "is_user_login"
    const val ACCESS_TOKEN = "access_token"
    const val REFRESH_TOKEN = "refresh_token"
    const val USER = "user"

    var homeFragments = arrayListOf("MainFragment","BlogListingFragment","JobSaveFragment")

    //Onboarding
    const val PASSWORD_LENGTH = 8

    // 3 SECONDS
    var LUNCH_SCREEN_DELAY_TIME: Long = 2000
    const val LOADING_SCREEN_LOGO_DELAY: Long = 2000

    //Basics
    var TERMS_CONDITON = "terms_condition"; var PRIVACY = "privacy_policy";

    //Patterns
    val specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE)
    val UpperCasePatten = Pattern.compile("[A-Z ]")
    val lowerCasePatten = Pattern.compile("[a-z ]")
    val digitCasePatten = Pattern.compile("[0-9 ]")
}