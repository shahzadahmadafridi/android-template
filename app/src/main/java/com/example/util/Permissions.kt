package com.example.Template.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

object Permissions {

    var LOCATION_PERMISSION_CODE = 101
    var DEVICE_ID_PERMISSION_CODE = 102
    var READ_EXTERNAL_STORAGE_CODE = 103

    fun checkLocationPermission(activityCompat: AppCompatActivity): Boolean {
        return if (ActivityCompat.checkSelfPermission(activityCompat, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestLocationPermission(activityCompat)
            false
        }
    }

    fun requestLocationPermission(activityCompat: AppCompatActivity) {
        ActivityCompat.requestPermissions(activityCompat, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_CODE)
    }

    fun checkDeviceIdPermission(activityCompat: AppCompatActivity): Boolean {
        return if (ActivityCompat.checkSelfPermission(activityCompat, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestDeviceIdPermission(activityCompat)
            false
        }
    }

    fun requestDeviceIdPermission(activityCompat: AppCompatActivity) {
        ActivityCompat.requestPermissions(activityCompat, arrayOf(Manifest.permission.READ_PHONE_STATE), DEVICE_ID_PERMISSION_CODE)
    }

    fun checkReadExternalStoragePermission(activityCompat: AppCompatActivity): Boolean {
        return if (ActivityCompat.checkSelfPermission(activityCompat, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestReadExternalStoragePermission(activityCompat)
            false
        }
    }

    fun requestReadExternalStoragePermission(activityCompat: AppCompatActivity) {
        ActivityCompat.requestPermissions(activityCompat, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), READ_EXTERNAL_STORAGE_CODE)
    }
}

