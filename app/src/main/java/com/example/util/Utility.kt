package com.example.Template.util

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.*
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.content.res.ColorStateList
import android.database.Cursor
import android.graphics.BlendMode
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.SystemClock
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.text.Spanned
import android.text.format.DateUtils
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.example.Template.BuildConfig
import com.example.Template.R
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


object Utility {

    /**
        TEST
     * Utilites method for full screen/in screen (status/navigation exclude)
     * Uses for softinput to adjust layout when keybaod open/close
     */

    fun onSystemUiTransparentNoLimit(window: Window){
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
    }

    fun onSystemUiTransparentInScreen(window: Window){
        window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
//        window.setFlags(
//            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
//            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
//        )
    }

    /**
     * Set system status/navigation color manually.
     */

    fun onStatusColor(parentActivity: AppCompatActivity, color: Int){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            parentActivity.window.statusBarColor = ContextCompat.getColor(
                parentActivity,
                color
            )
        }
    }

    fun onNavigationColor(parentActivity: AppCompatActivity, color: Int){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            parentActivity.window.navigationBarColor = ContextCompat.getColor(
                parentActivity,
                color
            )
        }
    }

    /**
     * Kotlin extension function to change radio button circle color programmatically
     */

    fun RadioButton.setCircleColor(color: Int) {
        val colorStateList = ColorStateList(
            arrayOf(
                intArrayOf(-android.R.attr.state_checked), // unchecked
                intArrayOf(android.R.attr.state_checked) // checked
            ), intArrayOf(
                Color.GRAY, // unchecked color
                color // checked color
            )
        )
        // finally, set the radio button's button tint list
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            buttonTintList = colorStateList
        }

        // optionally set the button tint mode or tint blend mode
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            buttonTintBlendMode = BlendMode.SRC_IN
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                buttonTintMode = PorterDuff.Mode.SRC_IN
            }
        }
        invalidate() //could not be necessary
    }

    /**
     * CHECK WHETHER INTERNET CONNECTION IS AVAILABLE OR NOT
     * 0: No Internet available (maybe on airplane mode, or in the process of joining an wi-fi).
     * 1: Cellular (mobile data, 3G/4G/LTE whatever).
     * 2: Wi-fi.
     * 3: VPN
     */

    @Suppress("DEPRECATION")
    fun Context.checkNetworkType(context: Context): Int {
        var result = 0 // Returns connection type. 0: none; 1: mobile data; 2: wifi
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cm?.run {
                cm.getNetworkCapabilities(cm.activeNetwork)?.run {
                    if (hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2
                    } else if (hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1
                    } else if (hasTransport(NetworkCapabilities.TRANSPORT_VPN)){
                        result = 3
                    }
                }
            }
        } else {
            cm?.run {
                cm.activeNetworkInfo?.run {
                    if (type == ConnectivityManager.TYPE_WIFI) {
                        result = 2
                    } else if (type == ConnectivityManager.TYPE_MOBILE) {
                        result = 1
                    } else if(type == ConnectivityManager.TYPE_VPN) {
                        result = 3
                    }
                }
            }
        }
        return result
    }

    @Suppress("DEPRECATION")
    fun Context.checkNetworkConnected(): Boolean {
        var result = false
        val cm = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    result = true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    result = true
                }
            }
        } else {
            val activeNetwork = cm.activeNetworkInfo
            if (activeNetwork != null) {
                // connected to the internet
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                    result = true
                } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                    result = true
                }
            }
        }
        return result
    }

    /**
     * CREATE CUSTOM DIALOG
     */

    fun onCreateDialog(context: Context, layout: Int, cancelable: Boolean): Dialog? {
        val metrics = context.resources.displayMetrics
        val width = metrics.widthPixels
        val height = metrics.heightPixels
        val dialog = Dialog(context, android.R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(layout)
        dialog.window!!.setGravity(Gravity.CENTER)
        dialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(cancelable)
        return dialog
    }

    fun onProgressDialog(context: Context, layout: Int): Dialog? {
        val dialog = Dialog(context, android.R.style.Theme_Dialog)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(layout)
        dialog.window!!.setGravity(Gravity.CENTER)
        dialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        val logo = dialog.findViewById<ImageView>(R.id.progressBar_logo)
        val animDrawable = logo.getBackground() as AnimationDrawable
        animDrawable.setEnterFadeDuration(2000)
        animDrawable.setExitFadeDuration(2000)
        animDrawable.start()
        return dialog
    }

    fun onProgressLayout(isShow: Boolean, layout: View) {
        val logo = layout.findViewById<ImageView>(R.id.progressBar_logo)
        val animDrawable = logo.background as AnimationDrawable
        animDrawable.setEnterFadeDuration(2000)
        animDrawable.setExitFadeDuration(2000)
        if(isShow){
            animDrawable.start()
            layout.visibility = View.VISIBLE
        }else{
            animDrawable.stop()
            layout.visibility = View.INVISIBLE
        }
    }

    /**
     * SHARE CONTENT WITH OTHER APPS
     */

    fun onShare(context: Context, str: String){
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, str)
        context.startActivity(Intent.createChooser(intent, "Share with"))
    }

    /**
     * BUTTON CLICK EFFECT
     */

    @SuppressLint("ClickableViewAccessibility")
    fun buttonEffect(button: View) {
        button.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                v.background.setColorFilter(-0x1f0b8adf, PorterDuff.Mode.SRC_ATOP)
                v.invalidate()
            }
            else if (event.action == MotionEvent.ACTION_UP) {
                v.background.clearColorFilter()
                v.invalidate()
            }
            false
        }
    }

    /**
     * GET CURRENT DATE e-g 31-12-2020
     */

    fun getCurrentDate(): String {
        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]

        val strDay: String = if (day < 10) {
            "0$day"
        } else {
            "" + day
        }

        return if ((month + 1) < 10) {
            "$strDay-0${month + 1}-$year"
        } else {
            "$strDay-${month + 1}-$year"
        }
    }

    /**
     * GET PDF FILE
     */

    fun getPdfFileChooser(): Intent {
        val mimeTypes = arrayOf("application/pdf")
        val intent = Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*|application/pdf")
                .putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        return intent
    }

    /**
     * GET PDF FILE NAME
     */

    fun getFileName(context: Context, uri: Uri): String? {
        var result: String?
        if (uri.scheme.equals("content")) {
            val cursor: Cursor? = context.getContentResolver().query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }

        result = uri.getPath()

        result?.let {
            val cut = it.lastIndexOf('/')
            if (cut != -1) {
                result = it.substring(cut + 1)
            }
        }

        return result
    }

    /**
     * GET PDF FILE PATH
     */

    fun getRealPathFromURI(context: Context, uri: Uri): String? {
            val cursor: Cursor? = context.getContentResolver().query(uri, null, null, null, null)
            return if (cursor == null) {
                uri.path
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }
    }

    fun getBlinkAnimamtion(context: Context): Animation{
       return AnimationUtils.loadAnimation(context, R.anim.blink)
    }

    /**
     * Extension function
     */

    fun TextView.underline() {
        this.paintFlags = Paint.UNDERLINE_TEXT_FLAG
    }

    fun String.isValidEmail() = isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

    fun EditText.placeCursorToEnd() {
        this.setSelection(this.text.length)
    }

    fun String?.toHtml(): Spanned? {
        if (this.isNullOrEmpty()) return null
        return HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_COMPACT)
    }

    fun String.toISODateFormate(): String{
        val tz = TimeZone.getTimeZone("UTC")
        val date = SimpleDateFormat("dd-MM-yyyy").parse(this)
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX") // Quoted "Z" to indicate UTC, no timezone offset
        df.timeZone = tz
        return df.format(date)
    }

    fun toCurrentISODateFormate(): String{
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") // Quoted "Z" to indicate UTC, no timezone offset
        df.timeZone = TimeZone.getTimeZone("UTC")
        return df.format(Date())
    }

    fun String.fromISODateFormate(): String{
        val tz = TimeZone.getTimeZone("UTC")
        var df: DateFormat?
        var date: Date?
        try{
            date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(this)
            df = SimpleDateFormat("dd-MM-yyyy") // Quoted "Z" to indicate UTC, no timezone offset
        }catch (e: ParseException){
            date = SimpleDateFormat("yyyy-MM-dd").parse(this)
            df = SimpleDateFormat("dd-MM-yyyy") // Quoted "Z" to indicate UTC, no timezone offset
        }
        df!!.timeZone = tz
        return df.format(date)
    }

    fun String.fromISODateTimeFormate(): String{
        val tz = TimeZone.getTimeZone("UTC")
        var vdf: DateFormat?
        var df: DateFormat?
        var date: Date?
        try{
            vdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            vdf.timeZone = tz
            date = vdf.parse(this)
            df = SimpleDateFormat("dd-MM-yyyy HH:mm:ss a", Locale.getDefault()) // Quoted "Z" to indicate UTC, no timezone offset
        }catch (e: ParseException){
            vdf = SimpleDateFormat("yyyy-MM-dd")
            vdf.timeZone = tz
            date = vdf.parse(this)
            df = SimpleDateFormat("dd-MM-yyyy HH:mm:ss a", Locale.getDefault()) // Quoted "Z" to indicate UTC, no timezone offset
        }
        df!!.timeZone = TimeZone.getDefault()
        return df.format(date)
    }

    fun String.fromISOtoRelativeTime(): String {
        val tz = TimeZone.getTimeZone("UTC")
        var df: DateFormat?
        val due: Date? = try {
            df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
            df.timeZone = tz
            df.parse(this)
        } catch (e: ParseException) {
            df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            df.timeZone = tz
            df.parse(this)
        }
        return DateUtils.getRelativeTimeSpanString(
            due!!.time,
            Date().time,
            0L,
            DateUtils.FORMAT_ABBREV_ALL
        ).toString()
    }

    fun String.fromISODateToDaysAgo(): String{
        val tz = TimeZone.getTimeZone("UTC")
        var df: DateFormat?

        val date = try{
            df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            df.timeZone = tz
            df.parse(this)
        }catch (e: ParseException){
            df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            df.timeZone = tz
            df.parse(this)
        }
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(Date().time - date!!.time)
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(Date().time - date.time)
        val hours: Long = TimeUnit.MILLISECONDS.toHours(Date().time - date.time)
        val days: Long = TimeUnit.MILLISECONDS.toDays(Date().time - date.time)

        Log.e("SplashActivity", " fromISODateToDaysAgo => seconds: $seconds minutes: $minutes hours: $hours days: $days")

        val daysAgoStr = DateUtils.getRelativeTimeSpanString(
            date.time,
            Calendar.getInstance().timeInMillis,
            DateUtils.MINUTE_IN_MILLIS
        ).toString()
        return daysAgoStr
    }

    fun View.onAnimateIv(icon: Int){
        this.animate().scaleX(0.5f).scaleY(0.5f).setDuration(200).withEndAction {
            this.animate().scaleX(1f).scaleY(1f).setDuration(300).withStartAction {
                (this as ImageView).setImageResource(icon)
            }
        }
    }

    fun View.onAnimateAlpha(visible: Boolean){
        when(visible){
            true -> this.animate().alpha(1f)
            false -> this.animate().alpha(0.0f)
        }
    }

    fun View.onVisibility(visible: Boolean){
        when(visible){
            true -> {
                this.animate().alpha(1f).start()
                this.visibility = View.VISIBLE
            }
            false -> {
                this.animate().alpha(0.0f).start()
                this.visibility = View.GONE
            }
        }
    }

    fun Context.hideKeyboard(view: View){
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun View.clickWithDebounce(debounceTime: Long = 2000L, action: () -> Unit) {
        this.setOnClickListener(object : View.OnClickListener {
            private var lastClickTime: Long = 0
            override fun onClick(v: View) {
                if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
                else action()
                lastClickTime = SystemClock.elapsedRealtime()
            }
        })
    }

    fun getPathFromURI(context: Context, contentUri: Uri?): String? {
        var cursor: Cursor = context.contentResolver.query(contentUri!!, null, null, null, null)!!
        cursor.moveToFirst()
        var document_id = cursor.getString(0)
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1)
        cursor.close()
        cursor = context.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            null,
            MediaStore.Images.Media._ID + " = ? ",
            arrayOf(
                document_id
            ),
            null
        )!!
        cursor.moveToFirst()
        val path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA))
        cursor.close()
        return path
    }

    /**
     * Utility methods shows logs/toast message.
     */

    fun showDebugLog(TAG: String, Msg: String){
        Log.d(TAG, Msg)
    }

    fun showErrorLog(TAG: String, Msg: String){
        Log.e(TAG, Msg)
    }

    fun showErrorLogException(TAG: String, Msg: String, exception: Exception?){
        Log.e(TAG, Msg, exception)
    }

    fun showToast(context: Context, message: String){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun provideSharedPref(context: Context): SharedPreferences{
        return context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE)
    }

    /**
     * Implicit Intent to share with other apps.
     */

    @SuppressLint("QueryPermissionsNeeded")
    fun startApplication(packageName: String?, parentActivity: AppCompatActivity) {
        var toast: Toast? = null
        try {
            val intent = Intent("android.intent.action.MAIN")
            intent.addCategory("android.intent.category.LAUNCHER")
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            val resolveInfoList: List<ResolveInfo> = parentActivity.packageManager.queryIntentActivities(
                intent,
                0
            )
            for (info in resolveInfoList)
                if (info.activityInfo.packageName.equals(
                        packageName,
                        ignoreCase = true
                    )
            ) {
                launchApp(info.activityInfo.packageName, info.activityInfo.name, parentActivity)
                return
            }
        } catch (e: Exception) {
            toast = Toast.makeText(parentActivity, "Can't find Gmail", Toast.LENGTH_LONG)
            toast.show()
        }
    }

    fun sendTweet(context: Context, msg: String) {
        var applicationStr =  "\t\t https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n"
        var message = msg + "\n\n" + Uri.parse(applicationStr)
        try {
            var intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, message)
                type = "text/plain"
                setPackage("com.twitter.android")
            }
            context.startActivity(intent)
        }catch (e: ActivityNotFoundException){
            val url = "https://twitter.com/intent/tweet?text=$message"
            var intent = Intent().apply {
                action = Intent.ACTION_VIEW
                data = Uri.parse(url)
            }
            context.startActivity(intent)
        }
    }

    private fun launchApp(packageName: String, name: String, parentActivity: AppCompatActivity) {
        val intent = Intent("android.intent.action.MAIN")
        intent.addCategory("android.intent.category.LAUNCHER")
        intent.component = ComponentName(packageName, name)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        parentActivity.startActivity(intent)
    }
}
